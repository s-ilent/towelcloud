﻿/*
	TowelCloud

	Copyright (c) 2020 towel_funnel

	This software is released under the Zlib License.
	https://opensource.org/licenses/zlib-license

	このコードはZlibライセンスです。
	https://ja.wikipedia.org/wiki/Zlib_License

	このコードはとくに改変をしない場合自由に使うことができます
	改変する場合の注意事項は上記Wikipediaを参考にしてください

	VRCワールドなど、利用した空間内で著作者表示をする義務はありません
	ただし、可能な範囲で書いておいてもらえると、使おうと思った他の人の検索の助けになるので嬉しいです！
*/

Shader "Towel/TowelCloud"
{
	Properties
	{
		// dev0
		[Toggle] _ForObject("オブジェクトとして配置する（skyboxにしない）", Float) = 0
		_scale ("雲の大きさ（小さいと繰り返しが目立つ）", Float) = 55
		_cloudy ("曇り度合い", Range (0, 1)) = 0.5
		_soft ("雲の柔らかさ", Range (0.0001, 0.9999)) = 0.1
		[Header(sky)]
		_skyMap ("空の色（上→空の上部, 左→空の前方）", 2D) = "white" {}
		[Toggle]_skyTransParent ("空を透過する", Float) = 0
		_rotateY ("空の前方の角度（0はX方向）", Range (-180, 180)) = 0
		_rotateZ ("光の上下角度（0は水平）", Range (-180, 180)) = 60
		[Header(cloud)]
		_cloudMap ("雲の色（上→雲の明るい部分, 左→空の前方）", 2D) = "white" {}
		_skyEffectRate ("空の色を雲に反映する割合", Range (0, 1)) = 0.5
		[Header(move)]
		_moveRotation ("雲の移動方向（デフォルトはX方向）", Range (0, 360)) = 0
		_speed ("雲の速度", Float) = 0.5
		_shapeSpeed ("雲の変形量", Float) = 0.1
		_speedOffset ("雲の細かい部分の速度差", Float) = 0.2
		_speedSlide ("雲の細かい部分の横方向への速度", Float) = 0.1
		[Header(rim)]
		_rimMap ("雲のふちの色（上→雲の明るい部分, 左→空の前方）", 2D) = "white" {}
		_rimForce ("ふちの光の強さ", Float) = 2.5
		_rimNarrow ("ふちの光の細さ", Float) = 1
		[Header(scattering)]
		[Toggle] _scattering ("拡散光を使う（太陽の方向で端っこが明るくなる）", Float) = 0
		_scatteringColor ("拡散光の色", Color) = (1, 1, 1, 1)
		_scatteringForce ("拡散光の強さ", Range (0, 3)) = 0.1
		_scatteringPassRate ("拡散光の貫通力", Range (0.05, 0.95)) = 0.1
		_scatteringRange ("拡散光の空に対する広さ", Range (0, 1)) = 0.1
		[Header(turbulence)]
		[Toggle] _turbulence ("乱流（雲を歪ませて上空に風があるように見える）", Float) = 1
		_turbulenceForce ("乱流の強さ", Float) = 5
		_turbulenceScale ("乱流の大きさ（サイズが小さすぎると渦のようになる）", Float) = 5
		_turbulenceMove ("乱流の変形速度", Float) = 1.5
		[Header(etc)]
		_fbmScaleUnder ("雲の細かい部分の変形値（小さいとうろこ雲になる）", Float) = 0.42
		_boost ("雲の光を強める値（高いとブルームの影響を受ける）", Float) = 1
		_softDecayArea ("くっきりさせる調節範囲", Range (0, 1)) = 0.5
		_softDecayPower ("くっきりさせる調節量", Range (0, 1)) = 0.5
		_alphaRate ("全体の透明度", Range (0, 1)) = 1
	}
	SubShader
	{
	// ==== head ====
		Tags
		{
			"RenderType"="Background"
			"Queue"="Transparent-100"
			"PreviewType"="SkyBox"
		}
		
		ZWrite On
		Blend SrcAlpha OneMinusSrcAlpha
	// 
		Pass
		{
		// ==== head ====
			Cull [_ForObject]
			CGPROGRAM
			#pragma shader_feature _FOROBJECT_ON
			#pragma shader_feature _TURBULENCE_ON
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			#include "lib/SimplexNoise3D.hlsl"
			#include "lib/Quaternion.hlsl"
			#include "lib/Easing.hlsl"
			
			#ifndef PI
			#define PI 3.14159265359f
			#endif
			
			
		// ==== セマンティクス ====
			struct appdata
			{
				float4 vertex : POSITION;
			};
			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 worldPos : TEXCOORD0;
			};
			
		// ==== パラメータ ====
			// dev1
			uniform float _scale;
			uniform float _cloudy;
			uniform float _soft;
			
			uniform sampler2D _skyMap;
			uniform float _skyTransParent;
			uniform float _rotateY;
			static const float _rotateYOffset = 0;
			uniform float _rotateZ;
			static const float _rotateZOffset = 0;
			
			uniform sampler2D _cloudMap;
			uniform float _skyEffectRate;
			
			uniform float _moveRotation;
			uniform float _speed;
			uniform float _shapeSpeed;
			uniform float _speedOffset;
			uniform float _speedSlide;
			
			uniform sampler2D _rimMap;
			uniform float _rimForce;
			uniform float _rimNarrow;
			
			uniform float _scattering;
			uniform float4 _scatteringColor;
			uniform float _scatteringForce;
			uniform float _scatteringPassRate;
			uniform float _scatteringRange;
			
			uniform float _turbulenceScale;
			uniform float _turbulenceForce;
			uniform float _turbulenceMove;
			
			uniform float _fbmScaleUnder;
			uniform float _boost;
			uniform float _softDecayArea;
			uniform float _softDecayPower;
			uniform float _alphaRate;
			
		// ==== var ====
			static const int noiseLoop = 6; // ノイズのループ数、高いと品質が上がるが処理が重くなる
			static const float km = 1000; // 値が大きすぎるのでひとまずkm表示
			static const float planetR_km = 6000;// 惑星半径（km表示）
			static const float cloudHeight_km = 20;// 雲の生成される地上からの高さ
			static const float adjustRate_km = 15;// 雲の調節処理が行われる基準値（低いほど細かく行われるが、非連続になる）
			static const float adjustOffset = 1; // 雲の調節処理を行わない回数（高いほど近くの雲で行わなくなる）
			static const float adjustMax = 5; // 雲の調節処理の最大数（高いと水平線以下に縞模様が出現、低いと水平線近くが細かくなってしまう）
			
		// ==== 数学関数 ====
			inline float remap(float value, float minOld, float maxOld, float minNew, float maxNew)
			{
				return minNew + (value - minOld) * (maxNew - minNew) / (maxOld - minOld);
			}
			
		// ==== vertex ====
			// vert
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);

				o.worldPos = mul(unity_ObjectToWorld, v.vertex);

				return o;
			}
		// ==== fragment関数 ====
			// slerp
			float3 slerpFloat3(float3 start, float3 end, float rate)
			{
				float _dot = dot(start, end);
				clamp(_dot, -1, 1);
				float theta = acos(_dot) * rate;
				float3 relative = normalize(end - start * _dot);
				return (start * cos(theta)) + (relative * sin(theta));
			}
			
			// 視点から見る先へのベクトルの生成
			inline float3 createReViewDir (float3 worldPos)
			{
				float3 worldViewDir = UnityWorldSpaceViewDir(worldPos.xyz);
				worldViewDir = normalize(worldViewDir);
				return worldViewDir;
			}
			
			// ノイズを組み合わせる
			float4 createFbm (float3 coord, float totalScale, float3 totalOffset, float3 fbmOffset, float3 speed, float fbmAdjust)
			{
				// 変数宣言
				float3 offset;
				// fbm変動量
				float swingRate = 2;
				// fbm変動値
				float swing = 1;
				float nowSwing;
				float smallness = 1; // reScale
				// 振幅合計
				float totalSwing = 0;
				// 引数整理
				float smallnessRate = _fbmScaleUnder;
				float totalSmallness = 1 / totalScale * pow(smallnessRate, -noiseLoop);
				// ノイズ本体
				float4 noise = float4(0, 0, 0, 0);
				float4 nowNoise;
				// 前処理
				float baseOffset = totalScale * 10;
				coord.xz += totalOffset;
				fbmOffset /= noiseLoop;
				
				// adjast
				float adjustBase = floor(fbmAdjust);
				float adjustSmallRate = frac(fbmAdjust);
				float adjustLargeRate = 1 - adjustSmallRate;
				smallness = pow(smallnessRate, adjustBase);
				// 最初のループ値の打ち消し
				swing /= swingRate;
				smallness /= smallnessRate;
				// fbmを生成
				for (int loopIndex = 0; loopIndex < noiseLoop; loopIndex++)
				{
					float adjustIndex = adjustBase + loopIndex;
					// ループ値
					swing *= swingRate;
					nowSwing = swing;
					if (loopIndex == 0)
					{
						nowSwing *= adjustLargeRate;
					}
					if (loopIndex == noiseLoop - 1)
					{
						nowSwing *= adjustSmallRate;
					}
					smallness *= smallnessRate;
					// ノイズの生成
					offset = -fbmOffset * length(speed.xz) * (adjustIndex - noiseLoop) + speed + baseOffset;
					// 2d
					// nowNoise.xzw = snoise2d_grad(((coord.xz + offset) * smallness * totalSmallness));
					nowNoise = snoise3d_grad((coord + offset) * smallness * totalSmallness);
					// noiseの最大値に近い場合頂点の近くにあると推測する。値に応じてViewDirの値を補完で強めて合成する
					nowNoise.y = quadInOut(nowNoise.w);
					noise += nowNoise * nowSwing;
					// 振幅合計を記録
					totalSwing += nowSwing;
				}
				// 合計振幅で減衰させる
				noise /= totalSwing;
				// 
				return noise;
			}
			
			// １段階のみのノイズ
			float4 createSingle (float3 coord, float totalScale, float3 totalOffset, float3 speed)
			{
				float3 offset;
				float baseOffset = totalScale * 10;
				float totalSmallness = 1 / totalScale;
				// ノイズ本体
				offset = speed + baseOffset;
				float4 noise = snoise3d_grad((coord + offset) * totalSmallness);
				return noise;
			}
		// ==== fragment ====
			// frag
			fixed4 frag (v2f i) : SV_Target
			{
			// == head
				UNITY_SETUP_INSTANCE_ID(i);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
				// 必要な書き下し
				float3 worldPos = i.worldPos.xyz;
				
			// == 前処理
				float planetR = planetR_km * km;
				float cloudHeight = cloudHeight_km * km;
				float adjustRate = adjustRate_km * km;
				float3 viewDir = -normalize(mul((float3x3)unity_ObjectToWorld, i.worldPos.xyz));
				float3 reViewDir = -viewDir;
				float vy = abs(reViewDir.y);
				float totalR = cloudHeight + planetR;
				
			// == 半円天球を計算
				// 視点を半球まで伸ばすまでの比率の計算
				float viewDistance = sqrt(totalR * totalR - (1 - vy * vy) * (planetR * planetR)) - vy * planetR;
				// 円の計算
				float3 ovalCoord = reViewDir * viewDistance;
				// 距離の算出
				float adjustBase = pow((length(ovalCoord)) / adjustRate, 0.6); // sqrtはちょうどよいカーブを作るためであり、ルートの意味は無い
				// adjustBase = min(adjustBase, 10);
				adjustBase = clamp(adjustBase - adjustOffset, 0, adjustMax);
			// == ノイズの生成
				// オフセット値を計算
				float4 moveQuaternion =  rotate_angle_axis(_moveRotation * PI / 180, float3(0, 1, 0));
				float3 fbmOffset = float3(_speedOffset, 0, _speedSlide);
				float3 speed = _Time.y * float3(_speed, _shapeSpeed, 0) * km;
				speed = rotate_vector(speed, moveQuaternion);
				fbmOffset = rotate_vector(fbmOffset, moveQuaternion);
				
			// == 乱流を作成
				#ifdef _TURBULENCE_ON
					float3 turbulenceSpeed = speed * _turbulenceMove;
					float4 turbulenceNoise = createSingle(ovalCoord, _turbulenceScale * _scale * km, 1 * km, turbulenceSpeed);
					// 乱流値から座標をずらす
					ovalCoord += turbulenceNoise.xyz * _turbulenceForce * km;
				#endif
					
			// == 実ノイズを生成;
				float4 noise = createFbm(ovalCoord, _scale * km, 1 * km, fbmOffset, speed, adjustBase);
				// ノイズの最後が強度の値になる
				float cloudNoisePower = clamp(noise.w,-1, 1) * 0.5 + 0.5;
				
			// == 距離からソフト値を減衰
				float soft = tan((1 - _soft) * PI / 2);
				float3 topVector = float3(0, 1, 0);
				float softDecay = clamp(dot(reViewDir, topVector), 0, 1);
				softDecay = remap(softDecay, 1 - _softDecayArea, 1, 0, _softDecayPower);
				softDecay = 1 - clamp(softDecay, 0, 1);
				soft /= softDecay;
				
			// == ソフト値と曇度から濃さを算出
				// float cloudPower = remap(cloudNoisePower, 1 - _cloudy - soft / 2, 1 - _cloudy + soft / 2, 0, 1);
				float cloudPower = soft * cloudNoisePower + 0.5 - (1 - _cloudy) * soft;
				cloudPower = cubicInOut(clamp(cloudPower, 0, 1));
				
				// ノーマル方向をフォース空間から求める
				// フォース空間の求める先が雲の中央と仮定し、その逆方向のノーマライズを雲表面とすることができる
				// ただし、求まる値は大気面の切り取られた値なので、雲の濃い部分はより正面向きであるという補正が必要になる
				float3 cloudWorldNormal = normalize(-noise.xyz);
				// 雲の存在する範囲のうちの割合
				float cloudAreaRate = (cloudNoisePower + _cloudy - 1) / _cloudy;
			// == ビューに合わせてノーマル方向を回転する
				float3 underVector = float3(0, -1, 0);
				float4 viewQuaternion = from_to_rotation(underVector, viewDir);
				cloudWorldNormal = rotate_vector(cloudWorldNormal, viewQuaternion);
			// == 光の方向を回転する
				// 光の方向を用意、基準はX+方向
				float3 lightVector = float3(1, 0, 0);
				float3 skyVector = float3(1, 0, 0);
				float4 quaternionY =  rotate_angle_axis((_rotateY - _rotateYOffset) * PI / 180, float3(0, 1, 0));
				float4 quaternionZ =  rotate_angle_axis((_rotateZ - _rotateZOffset) * PI / 180, float3(0, 0, 1));
				lightVector = rotate_vector(lightVector, quaternionZ);
				lightVector = rotate_vector(lightVector, quaternionY);
				skyVector = rotate_vector(skyVector, quaternionY);
				// それぞれのdotから光影響を計算
				float normalDot = dot(cloudWorldNormal, lightVector);
				float normalDotForUv = normalDot * 0.5 + 0.5;
				float viewDotToLightForUv = dot(viewDir, lightVector) * 0.5 + 0.5;
				float viewDotToSkyForUv = dot(viewDir, skyVector) * 0.5 + 0.5;
			// == 雲の色を確定
				float2 cloudUv = float2(viewDotToLightForUv, normalDotForUv);
				float4 cloudColor = tex2D(_cloudMap, cloudUv);
			// == 空の色を用意
				float2 skyUv = float2(viewDotToSkyForUv, clamp(reViewDir.y, 0, 1));
				float4 skyColor = tex2D(_skyMap, skyUv);
				if (_skyTransParent == 1) skyColor.a = 0;
			// == 空の上下平均色を合成する
				float2 skyCenterUv = float2(viewDotToSkyForUv, 0.5);
				float4 skyCenterColor = tex2D(_skyMap, skyCenterUv);
				float3 skyEffect = skyColor.rgb - skyCenterColor.rgb;
				cloudColor.rgb += skyEffect * _skyEffectRate;
			// == 境界が光る度合いを用意
				float rimPower = 1 - circOut(cloudAreaRate);
				rimPower = pow(rimPower, _rimNarrow);
				float2 rimUv = float2(viewDotToLightForUv, normalDotForUv);
				float3 rimAddColor =  tex2D(_rimMap, rimUv) * cloudPower * _rimForce * rimPower;
			// == 透過拡散光
				float scatteringPower = 0;
				if (_scattering)
				{
					// 基本値をremapで算出
					scatteringPower = remap(cloudAreaRate, 0, _scatteringPassRate, 1, 0) * _scatteringForce;
					scatteringPower = max(scatteringPower, 0); // 0以下は削除
					// 内側のフェードを丸くする
					scatteringPower = quadIn(scatteringPower);
					// 範囲を限定する
					scatteringPower *= quadIn(clamp((_scatteringRange - viewDotToLightForUv) / _scatteringRange, 0, 1));
					// 拡散光を追加
					cloudColor.rgb += (scatteringPower * _scatteringColor);
				}
			// == 最終合成
				float cloudAlpha = cloudColor.a * cloudPower;
				float endAlpha = clamp(skyColor.a + cloudAlpha, 0, 1);
				float alphaTotal = cloudAlpha + (1 - cloudAlpha) * skyColor.a;
				float cloudColorRate = cloudAlpha / alphaTotal;
				float skyColorRate = ((1 - cloudAlpha) * skyColor.a) / alphaTotal;
				float3 endRgb = cloudColor.rgb * cloudColorRate + skyColor.rgb * skyColorRate;
				// float alphaDiv = skyColor.a + cloudAlpha;
				// float3 endRgb = skyColor.rgb * skyColor.a / alphaDiv + cloudColor.rgb * cloudAlpha / alphaDiv;
				float4 endColor = float4(0, 0, 0, 1);
				endColor.rgb = endRgb + rimAddColor;
				endColor *= _boost;
			// == アルファを使うかどうか分岐
				#ifdef _FOROBJECT_ON
					endColor.a = endAlpha * _alphaRate;
				#else
					endColor.a = endAlpha;
				#endif
				
				// dev2
			// == end
				return endColor;
			}
			ENDCG
		}
	}
}